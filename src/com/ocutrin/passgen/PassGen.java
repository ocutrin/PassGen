package com.ocutrin.passgen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public enum PassGen {

	GENERATE;

	public String genPass(int lenght) {
		return genPass(lenght, PassChar.ALL);
	}

	public String genPass(int lenght, String passExtras) {
		return genPass(lenght, passExtras, PassChar.ALL);
	}

	public String genPass(int lenght, ThreadLocalRandom threadLocalRandom) {
		return genPass(lenght, threadLocalRandom, PassChar.ALL);
	}

	public String genPass(int lenght, PassChar... passChars) {
		return genPass(lenght, ThreadLocalRandom.current(), "", passChars);
	}

	public String genPass(int lenght, String passExtras, PassChar... passChars) {
		return genPass(lenght, ThreadLocalRandom.current(), passExtras, passChars);
	}

	public String genPass(int lenght, ThreadLocalRandom threadLocalRandom, PassChar... passChars) {
		return genPass(lenght, threadLocalRandom, "", passChars);
	}

	public String genPass(int lenght, ThreadLocalRandom threadLocalRandom, String passExtras, PassChar... passChars) {
		PassCheck.threadLocalRandomNotNull(threadLocalRandom);
		PassCheck.passExtrasNotNull(passExtras);
		PassCheck.passCharsNotNull(passChars);
		return genPassPrivado(lenght, threadLocalRandom, passExtras,
				Arrays.asList(passChars).stream().map(PassChar::getChars).reduce("", String::concat));
	}

	private static String genPassPrivado(int lenght, ThreadLocalRandom threadLocalRandom, String passExtras,
			String passChars) {
		return new ArrayList<String>(Arrays.asList(new String[lenght])).stream().map(
				s -> String.valueOf(passChars.toCharArray()[threadLocalRandom.nextInt(passChars.toCharArray().length)]))
				.reduce("", String::concat).concat((passExtras));
	}

}
