package com.ocutrin.passgen;

public class PassError extends Error {

	private static final long serialVersionUID = 211462976801768075L;

	public PassError(String message) {
		super(message);
	}

}
