package com.ocutrin.passgen;

import java.util.Locale;

public enum PassChar {

	CHARS_UPPER_CASE,

	CHARS_LOWER_CASE,

	CHARS,

	NUMBERS,

	CHARS_NUMBERS,

	SPECIALS,

	ALL;

	public String getChars() {
		Lenguaje lenguaje = PassLocale.getLenguaje(Locale.getDefault());
		switch (this) {
		case CHARS_UPPER_CASE:
			return lenguaje.getMayusculas();
		case CHARS_LOWER_CASE:
			return lenguaje.getMinusculas();
		case CHARS:
			return CHARS_UPPER_CASE.getChars() + CHARS_LOWER_CASE.getChars();
		case NUMBERS:
			return lenguaje.getNumeros();
		case CHARS_NUMBERS:
			return CHARS.getChars() + NUMBERS.getChars();
		case SPECIALS:
			return lenguaje.getEspeceiales();
		default:
			return CHARS.getChars() + NUMBERS.getChars() + SPECIALS.getChars();
		}
	}
}