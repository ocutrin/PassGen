package com.ocutrin.passgen;

import java.util.Arrays;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.ocutrin.passgen.Lenguaje.Letras;

public class PassLocale {

	static public void main(String[] args) {
		showTodosLenguajes();
	}

	static public void showTodosLenguajes() {
		getTodosLenguajes().forEach(System.out::println);
	}

	static public void showLenguaje(Locale locale) {
		System.out.println(getLenguaje(locale).toString());
	}

	static public Set<Lenguaje> getTodosLenguajes() {
		return Arrays.asList(Locale.getAvailableLocales()).stream().filter(PassLocale::filtroLenguajes)
				.collect(Collectors.toSet()).stream().map(PassLocale::getLenguaje).collect(Collectors.toSet());
	}

	static private boolean filtroLenguajes(Locale locale) {
		return locale.getCountry() == null || locale.getCountry().isEmpty() || locale.getDisplayLanguage().isEmpty();
	}

	static public Lenguaje getLenguaje(Locale locale) {
		return new Lenguaje(locale.getDisplayLanguage(), getNumeros(), getLetras(locale), getSpeciales());
	}

	static private Letras getLetras(Locale locale) {
		return new Letras(getChar('a', 'z'), getChar('A', 'Z'));
	}

	static private String getNumeros() {
		return getChar('0', '9');
	}

	static private String getChar(char ini, char fin) {
		return IntStream.range((int) ini, (int) fin).mapToObj(i -> String.valueOf((char) i))
				.collect(Collectors.joining());
	}

	static private String getSpeciales() {
		return IntStream.range(32, 128).mapToObj(i -> (char) i).filter(PassLocale::filtroEspeciales).map(String::valueOf)
				.collect(Collectors.joining());
	}

	static private boolean filtroEspeciales(char c) {
		return !isNumero(c) && !isMayuscula(c) && !isMinuscula(c);
	}

	static private boolean isMinuscula(char c) {
		return c >= 'a' && c <= 'z';
	}

	static private boolean isMayuscula(char c) {
		return c >= 'A' && c <= 'Z';
	}

	static private boolean isNumero(char c) {
		return c >= '0' && c <= '9';
	}
}
