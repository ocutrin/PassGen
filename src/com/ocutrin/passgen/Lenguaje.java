package com.ocutrin.passgen;

public class Lenguaje {

	protected String nombre;
	protected Letras letras;
	protected String especeiales;
	protected String numeros;

	protected Lenguaje(String nombre, String numeros, Letras letras, String especeiales) {
		super();
		this.nombre = nombre;
		this.numeros = numeros;
		this.letras = letras;
		this.especeiales = especeiales;
	}

	public String getNombre() {
		return nombre;
	}

	public String getNumeros() {
		return numeros;
	}

	public String getMinusculas() {
		return letras.minusculas;
	}

	public String getMayusculas() {
		return letras.mayusculas;
	}

	public String getEspeceiales() {
		return especeiales;
	}

	@Override
	public String toString() {
		return "Lenguaje [nombre=" + nombre + ", numeros=" + numeros + ", minusculas=" + letras.minusculas
				+ ", mayusculas=" + letras.mayusculas + ", especiales=" + especeiales + "]";
	}

	static protected class Letras {
		protected String minusculas;
		protected String mayusculas;
		protected Letras(String minusculas, String mayusculas) {
			super();
			this.minusculas = minusculas;
			this.mayusculas = mayusculas;
		}
		
	}

}

