package com.ocutrin.passgen;

import java.util.concurrent.ThreadLocalRandom;

public class PassCheck {

	private PassCheck() {
	}

	public static void passExtrasNotNull(String passChars) {
		if (passChars == null)
			throw new PassError("PassExtras null.");
	}

	public static void passCharsNotNull(PassChar... passChars) {
		if (passChars == null)
			throw new PassError("PassChars null.");
	}

	public static void threadLocalRandomNotNull(ThreadLocalRandom threadLocalRandom) {
		if (threadLocalRandom == null)
			throw new PassError("ThreadLocalRandom null.");
	}

}
